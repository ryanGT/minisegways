#include <Wire.h>
#include <math.h>
const int MPU_addr=0x68;
double AcX,AcY,AcZ,Tmp,GyX,GyY,GyZ; //These will be the raw data from the MPU6050.
double AcX_offset;
uint32_t timer; //it's a timer, saved as a large, unsigned int.  We use it to save times from the "micros()" command and subtract the present time in microseconds from the time stored in timer to calculate the time for each loop.
uint32_t t0;
double compAngleX, compAngleY; //These are the angles in the complementary filter
#define degconvert 57.2957786 //there are like 57 degrees in a radian.

float kp=3.0;
int e;
int amp;

//#define enc1A 2
//#define enc1B 11
//
//#define enc2A 3
//#define enc2B 12
//
//int pwm_pin1 = 9;
//int m1in1 = 6;
//int m1in2 = 4;
//
////pwm_pin2, m2in1, m2in2
//int pwm_pin2 = 10;//<-- buy batteries and check these
//int m2in1 = 7;//<-- buy batteries and check these
//int m2in2 = 8;//<-- buy batteries and check these
#define stop_n 1000
#define encoderPinA 3//probably not right for you
#define encoderPinB 12//probably not right for you

int thd[stop_n];

int pwn_pin = 10;
int in1 = 7;
int in2 = 8;


//  encoder
volatile bool _EncoderBSet;
volatile long encoder_count = 0;

float t_ms;
int n_loop;

int fresh, nISR;

int inByte;
int state=0;
int ax_offset=0;

int N1 = 150;

int average_ax(){
  long ax_sum=0;
  int offset=0;
  int num=100;
  int k=0;

  for (k=0; k < num; k++){
        //Collect raw data from the sensor.
      Wire.beginTransmission(MPU_addr);
      Wire.write(0x3B);  // starting with register 0x3B (ACCEL_XOUT_H)
      Wire.endTransmission(false);
      Wire.requestFrom(MPU_addr,14,true);  // request a total of 14 registers
      AcX=Wire.read()<<8|Wire.read();  // 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)     
      AcY=Wire.read()<<8|Wire.read();  // 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
      AcZ=Wire.read()<<8|Wire.read();  // 0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)
      Tmp=Wire.read()<<8|Wire.read();  // 0x41 (TEMP_OUT_H) & 0x42 (TEMP_OUT_L)
      GyX=Wire.read()<<8|Wire.read();  // 0x43 (GYRO_XOUT_H) & 0x44 (GYRO_XOUT_L)
      GyY=Wire.read()<<8|Wire.read();  // 0x45 (GYRO_YOUT_H) & 0x46 (GYRO_YOUT_L)
      GyZ=Wire.read()<<8|Wire.read();  // 0x47 (GYRO_ZOUT_H) & 0x48 (GYRO_ZOUT_L)
    
      ax_sum = ax_sum + AcX;

      delay(10);
  }

  offset = ax_sum/num;
  return(offset);
}

void mynewline(){
  Serial.print('\n');
}

void command_motor(int speed){
  if (speed > 0){
    digitalWrite(in1, LOW);
    digitalWrite(in2, HIGH);
    analogWrite(pwn_pin, speed);
  }
  else if (speed < 0){
    digitalWrite(in1, HIGH);
    digitalWrite(in2, LOW);
    analogWrite(pwn_pin, abs(speed));
  }
  else {
    digitalWrite(in1, LOW);
    digitalWrite(in2, LOW);
    analogWrite(pwn_pin, 0);
  }
}
					  


void setup() {
  // Set up MPU 6050:
  Wire.begin();
  #if ARDUINO >= 157
  Wire.setClock(400000UL); // Set I2C frequency to 400kHz
  #else
    TWBR = ((F_CPU / 400000UL) - 16) / 2; // Set I2C frequency to 400kHz
  #endif

  pinMode(encoderPinA, INPUT); 
  pinMode(encoderPinB, INPUT); 
  // turn on pullup resistors
  digitalWrite(encoderPinA, HIGH);
  digitalWrite(encoderPinB, HIGH);

  attachInterrupt(digitalPinToInterrupt(encoderPinA), doEncoder, RISING);

  float dt = 1.0/N1;
  int i;
  int offset = 20;
  for (i=0; i < offset; i++){
    thd[i] = 0;
  }
  for (i=0; i < N1; i++){
    thd[i+offset] = 40*sin(2*PI*i*dt);
  }
  for (i=N1+offset; i < stop_n; i++){
    thd[i] = 0;
  }
  Wire.beginTransmission(MPU_addr);
  Wire.write(0x6B);  // PWR_MGMT_1 register
  Wire.write(0);     // set to zero (wakes up the MPU-6050)
  Wire.endTransmission(true);
  Serial.begin(115200);
  delay(100);
  Serial.print("DC Motor MPU 6050");
  //setup starting angle
  //1) collect the data
  Wire.beginTransmission(MPU_addr);
  Wire.write(0x3B);  // starting with register 0x3B (ACCEL_XOUT_H)
  Wire.endTransmission(false);
  Wire.requestFrom(MPU_addr,14,true);  // request a total of 14 registers
  AcX=Wire.read()<<8|Wire.read();  // 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)     
  AcY=Wire.read()<<8|Wire.read();  // 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
  AcZ=Wire.read()<<8|Wire.read();  // 0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)
  Tmp=Wire.read()<<8|Wire.read();  // 0x41 (TEMP_OUT_H) & 0x42 (TEMP_OUT_L)
  GyX=Wire.read()<<8|Wire.read();  // 0x43 (GYRO_XOUT_H) & 0x44 (GYRO_XOUT_L)
  GyY=Wire.read()<<8|Wire.read();  // 0x45 (GYRO_YOUT_H) & 0x46 (GYRO_YOUT_L)
  GyZ=Wire.read()<<8|Wire.read();  // 0x47 (GYRO_ZOUT_H) & 0x48 (GYRO_ZOUT_L)

  //2) calculate pitch and roll
  double roll = atan2(AcY, AcZ)*degconvert;
  double pitch = atan2(-AcX, AcZ)*degconvert;

  //3) set the starting angle to this pitch and roll
  double gyroXangle = roll;
  double gyroYangle = pitch;
  double compAngleX = roll;
  double compAngleY = pitch;

  //=======================================================
  // set up the Timer3 interrupt
  //=======================================================
  cli();          // disable global interrupts
  TCCR3A = 0;     // set entire TCCR1A register to 0
  TCCR3B = 0;     // same for TCCR1B

  // set compare match register to desired timer count:
  //OCR1A = 15624;
  //OCR3A = 155;//100 Hz
  OCR3A = 76;//100 Hz
  // OCR1A = 100;//150ish - seems to work
  //OCR1A = 77;//200 Hz <-- seems very borderline (might be 184 Hz)
  //OCR1A = 30;//500 Hz
  //OCR1A = 15;//1000 Hz
  //OCR1A = 7;//2000 Hz


  // turn on CTC mode:
  TCCR3B |= (1 << WGM12);

  // Set CS10 and CS12 bits for 1024 prescaler:
  TCCR3B |= (1 << CS10);
  TCCR3B |= (1 << CS12);
 
  // enable timer compare interrupt:
  TIMSK3 |= (1 << OCIE3A);

  sei();
  //=======================================================


  //start a timer
  timer = micros();
  state = 0;
  fresh = 0;
  nISR = 0;
}

void menu(){
  Serial.println("input s to start a test, z to zero");
  mynewline();
  state = 0;// If something goes wrong, set code to return to menu

  while (Serial.available() == 0){
      delay(10);
  }

  inByte = Serial.read();

  if ( inByte == 's' ){
    n_loop = 0;
    state = 1;//state = 1 --> run test; state = 0 --> back to menu
    t0 = micros();
    fresh = 0;
    nISR = 0;
    encoder_count = 0;
    amp = 0;
  }
  else if ( inByte == 'z' ){
    ax_offset = average_ax();
    compAngleY = 0;
    Serial.println("zeroing done");
    Serial.print("ax_offset = ");Serial.println(ax_offset);
  }
}

// Interrupt service routines for the right motor's quadrature encoder
void doEncoder()
{
  // Test transition; since the interrupt will only fire on 'rising' we don't need to read pin A
  //n++;

  _EncoderBSet = digitalRead(encoderPinB);   // read the input pin
  
  // and adjust counter + if A leads B
  if (_EncoderBSet){
    encoder_count ++;
  }
  else {
    encoder_count --;
  }
}
					  

void loop() {
  if ( fresh == 1 ){
    fresh = 0;

    if ( state == 0){
      menu();
    }
      
    else{
      if (n_loop == 0){
	t0 = micros();
      }
      double dt = (double)(micros() - timer) / 1000000; //This line does three things: 1) stops the timer, 2)converts the timer's output to seconds from microseconds, 3)casts the value as a double saved to "dt".
      timer = micros(); //start the timer again so that we can calculate the next dt.
      t_ms = (timer-t0)/1000.0;
      
      n_loop++;

      e = thd[n_loop]-encoder_count;
      amp = kp*e;
      if ( amp > 0 ){
	amp = amp + 18;
      }
      else if ( amp < 0 ){
	amp = amp - 18;
      }
      if ( amp > 255){
	amp = 255;
      }
      else if ( amp < -255 ){
	amp = -255;
      }
      command_motor(amp);
      
      //Now begins the main loop. 
      //Collect raw data from the sensor.
      Wire.beginTransmission(MPU_addr);
      Wire.write(0x3B);  // starting with register 0x3B (ACCEL_XOUT_H)
      Wire.endTransmission(false);
      Wire.requestFrom(MPU_addr,14,true);  // request a total of 14 registers
      AcX=Wire.read()<<8|Wire.read();  // 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)     
      AcY=Wire.read()<<8|Wire.read();  // 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
      AcZ=Wire.read()<<8|Wire.read();  // 0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)
      Tmp=Wire.read()<<8|Wire.read();  // 0x41 (TEMP_OUT_H) & 0x42 (TEMP_OUT_L)
      GyX=Wire.read()<<8|Wire.read();  // 0x43 (GYRO_XOUT_H) & 0x44 (GYRO_XOUT_L)
      GyY=Wire.read()<<8|Wire.read();  // 0x45 (GYRO_YOUT_H) & 0x46 (GYRO_YOUT_L)
      GyZ=Wire.read()<<8|Wire.read();  // 0x47 (GYRO_ZOUT_H) & 0x48 (GYRO_ZOUT_L)
    
      AcX_offset = AcX - ax_offset;
      //the next two lines calculate the orientation of the accelerometer relative to the earth and convert the output of atan2 from radians to degrees
      //We will use this data to correct any cumulative errors in the orientation that the gyroscope develops.
      double roll = atan2(AcY, AcZ)*degconvert;
      double pitch = atan2(-(AcX_offset), AcZ)*degconvert;
    
      //The gyroscope outputs angular velocities.  To convert these velocities from the raw data to deg/second, divide by 131.  
      //Notice, we're dividing by a double "131.0" instead of the int 131.
      double gyroXrate = GyX/131.0;
      double gyroYrate = GyY/131.0;
      
  
      //THE COMPLEMENTARY FILTER
      // This filter calculates the angle based MOSTLY on integrating
      // the angular velocity to an angular displacement.
      //
      // dt, recall, is the time between gathering data from the MPU6050.
      // We'll pretend that the angular velocity has remained constant
      // over the time dt, and multiply angular velocity by 
      // time to get displacement.
      // The filter then adds a small correcting factor from
      // the accelerometer ("roll" or "pitch"), so the gyroscope
      // knows which way is down.
  
      // Calculate the angle using a Complimentary filter
      compAngleX = 0.99 * (compAngleX + gyroXrate * dt) + 0.01 * roll; 
      compAngleY = 0.97 * (compAngleY + gyroYrate * dt) + 0.03 * pitch; 
  
      Serial.print(n_loop);Serial.print(",");
      Serial.print(t_ms);Serial.print(",");
      //Serial.print(compAngleX);Serial.print(",");//not in a useful direction
      Serial.print(thd[n_loop]);Serial.print(",");
      Serial.print(encoder_count);Serial.print(",");
      Serial.print(amp);Serial.print(",");
      Serial.print(compAngleY);Serial.print(",");
      Serial.print(AcX_offset);Serial.print(",");
      Serial.print(AcZ);Serial.print(",");
      Serial.print(pitch);Serial.print(",");
      //Serial.print(GyX);Serial.print(",");
      Serial.print(GyY);
      //Serial.print(",");
      //Serial.print(GyZ);
      //Serial.print(",");
      //Serial.print(pitch);
      Serial.print("\n");
    }
  
    if ( n_loop >= (stop_n-1) ){
      // for the sake of the thd array, we need to stop at stop_n-1
      command_motor(0);
      state = 0;
    }
  
  }
}

ISR(TIMER3_COMPA_vect)
{
  nISR++;
  fresh = 1;
  /* if (ISRstate == 1){ */
  /*   ISRstate = 0; */
  /*   digitalWrite(sw_pin, LOW); */
  /* } */
  /* else{ */
  /*   ISRstate = 1; */
  /*   digitalWrite(sw_pin, HIGH); */
  /* } */
}
