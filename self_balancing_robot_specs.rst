===========================================
Self-Balancing Robot Project (Mini-Segways)
===========================================

EGR 345
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. include:: /Users/kraussry/git/report_generation/beamer_header.rst


Big Idea
==============

- create a self-balancing robot that can also perform some other tasks

  - line following
  - responding to remote commands
  - navigating a room
  - climbing a ramp

Final Competitions
==================

- line following race (required)
- room navigation competition
- ramp climbing competition

Line Following Race
===================

.. figure:: spec_figs/race_track.pdf

	    
The Good News
=============

- the self-balancing robot project will consume the remaining lab sessions
- there will be no more lab reports

  - there will be update/progress emails

Minimum Requirements
====================

In order to receive at least a grade of 70%, your robot must complete
a basic qualification test that includes all of the following:

- balance itself for 90 seconds while remaining within a specified area
- respond to a command to move forward to a designated stopping point
- respond to a command to move backward to a second designated
  stopping point

Qualification continued
=======================

.. r2b-columnset::
    :width: 0.97

    .. r2b-column::
        :width: 0.60

	.. figure:: spec_figs/qualitifcation_i.pdf
            :height: 2.0in	    

    .. r2b-column::
        :width: 0.40

	- robot placed on center of horizontal line
	- robot must stay within the dashed box
        - robot must respond to a command to move forward or backward
          to vertical lines



Qualification Requirements
==========================


Any of the following will result in a failure of the qualification test:

- the robot falls over
- the robot leaves the 2' by 3' dashed box
- the robot does not respond to the forward and backward commands
- the robot fails to stop with the line sensor over the top or bottom
  line

  - Note: the line sensor does not have to be in the center of the
    robot, as long as the robot remains within the dashed box

Qualification Results
==========================
    
**Note:** If your robot cannot complete the qualification test,
your team will receive less than 70% on the final project.


Extra Points
============

- all teams that pass the qualification test get a minimum grade of 70%
- you can increase your grade through "degree of difficulty/style points":

  - performance in the competition(s)
  - making significant improvements to the mechanical design of the robot
  - using :code:`cherrypy` or :code:`flask` to create a web interface to
    control the robot
  - trying novel sensor or actuator ideas

    - and documenting your work

Degree of Difficulty/Style Point continued
==========================================

- other ways to increase your grade:

  - using :math:`i^2c` between the RPi and Arduino
  - using multiple Arduinos and having them communicate with each
    other or the RPi
  - doing real-time, bidirectional communication
  - other novel things you come up with


Robot "Kit"
===========

Each team will receive:

.. r2b-columnset::
    :width: 0.95

    .. r2b-column::
        :width: 0.50

	- two motors
        - H-bridge shield
        - two wheels and two hubs
        - Arduino Mega knock-off
        - Raspberry Pi 3 and micro SD card

    .. r2b-column::
        :width: 0.50
	  	  
        - MPU 6050
        - two sonar sensors
        - line sensor array
        - caster
        - portable phone charger

Power Supply Note
=================

- the RPi requires a regulated 5V and at least 500 mA
- the Arduino can be powered through USB (regulated 5V) or the barrel
  connection

  - barrel connection can take 7-12 V

- the RPi can power the Arduino over USB, but not the other way around
- the motors need 9-12V for max speed
- **Conclusion:**

  - you will need batteries (AA or 9V or whatever) in addition to the
    cell phone battery charger


RPi Pin Connection Caution
==========================

- **never** connect the GPIO pins on the RPi directly to an Arduino

  - you will owe me $40

- the RPi runs at 3.3V while the Arduino runs at 5V
- you need a level shifter
- come see me for a level shifter and permission before you attempt
  this

Chassis
=======

- I will post my solidworks files

  - I am not an expert on design of 3D printing
  - there are warping issues
    
- it may be easier to make longer pieces out of sheet metal or aluminum
- I need to know soon how much sheet metal or aluminum to have Roy order
- I will put a survey on Blackboard

  - decide by tomorrow as a team how much metal you want in your chassis

Robot Requirements: Max. Dimensions
===================================

.. figure:: spec_figs/chassis_size.pdf


Robot Requirements: Max. Weight
===============================

- 7 lbs


Additional Requirements
=======================

- the robot must be open-loop unstable
- other than sensors, nothing can be below the axle
- no kick stands in the final competition or qualification
- nothing other than the two drive wheels can touch the ground during
  the competition and qualification


Sub Tasks
=========

- sensing tilt angle
- basic Raspberry Pi/Linux
- wireless control/communication
- driving in a straight line

  - revisit velocity control with :math:`p_0`

- line following
- balancing
