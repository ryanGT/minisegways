#include <NewPing.h>
#include <digcomp.h>
#include <Wire.h>

const int MPU_addr=0x68;
double AcX,AcY,AcZ,Tmp,GyX,GyY,GyZ; //These will be the raw data from the MPU6050.

double compAngleX, compAngleY; //These are the angles in the complementary filter
#define degconvert 57.2957786 //there are like 57 degrees in a radian.

float b_z[2] = {0.14558824, -0.0759804};
float a_z[2] = {1., 0.11372545};
float Gz_in[2];
float Gz_out[2];
dig_comp Gz(b_z, a_z, Gz_in, Gz_out, 2,2);

// first order lowpass filter
// 10 Hz
float b_filt[2] = {0.38586955,  0.38586955};
float a_filt[2] = {1., -0.22826091};

// 5 Hz
//float b_filt[2] = {0.23905722, 0.23905722};
//float a_filt[2] = {1., -0.52188555};

// 4 HZ
//float b_filt[2] = {0.20084864,  0.20084864};
//float a_filt[2] = {1., -0.59830271};
  
// 3 Hz
//float b_filt[2] = {0.15860014,  0.15860014};
//float a_filt[2] = {1., -0.68279972};

// 1 Hz
//float b_filt[2] = {0.0591174,  0.0591174};
//float a_filt[2] = {1., -0.88176521};

  
float filt_in[2];
float filt_out[2];

dig_comp lpfilt(b_filt, a_filt, filt_in, filt_out, 2,2);


unsigned char getsecondbyte(int input){
  unsigned char output;
  output = (unsigned char)(input >> 8);
  return output;
}

int reassemblebytes(unsigned char msb, unsigned char lsb){
  int output;
  output = (int)(msb << 8);
  output += lsb;
  return output;
}


unsigned long t1;
unsigned long t2;

// Encoder pins
// Verify that these are the same pins your encoder
// A and B channels are connected to
#define enc1A 2
#define enc1B 11

#define enc2A 3
#define enc2B 12

// Motor 1
//int pwn_pin = 9;
//int in1 = 2;
//int in2 = 4;
//
//#define encoderPinA 18
//#define encoderPinB 16

//Motor 2
//int pwn_pin = 10;
//int in1 = 7;
//int in2 = 8;
//
//#define encoderPinA 19
//#define encoderPinB 17

// PWM and enable pins for the L298 H bridge
// Verify that these are the same pins your
// H bridge is connected to
//int pwm_pin1 = 6;
//int in1 = 8;
//int in2 = 9;
int pwm_pin1 = 9;
int m1in1 = 6;
int m1in2 = 4;

//pwm_pin2, m2in1, m2in2
int pwm_pin2 = 10;//<-- buy batteries and check these
int m2in1 = 7;//<-- buy batteries and check these
int m2in2 = 8;//<-- buy batteries and check these

//int isr_pin = A0;
int trig1 = 24;
int echo1 = 22;
int trig2 = 28;
int echo2 = 26;

#define MAX_DISTANCE 100

NewPing sonar1(trig1, echo1, MAX_DISTANCE);
NewPing sonar2(trig2, echo2, MAX_DISTANCE);

int d1;
int d2;
int sonar_diff;
float filt_sonar;

int goaldelay = 20000;//20 milliseconds
int telapsed;

//  encoder
volatile bool _EncBSet1;
volatile long enc_count1 = 0;

volatile bool _EncBSet2;
volatile long enc_count2 = 0;

volatile int enc_pend = 0;

int n_loop, stop_n, n_delay, nISR;
long raw_loop_count=0;

const byte mask = B11111000;
int prescale = 1;

int fresh;
int ISRstate;

#define offset 0

int inByte;
int outByte;

int extra_param;
int amp;
float freq;
float w;
float theta_d;
float dt_ms;
float dt1;
float dt2;
float dt_sec;
float kp;
float kd;
float a;
int pwm1;
int pwm2;
int width;
int curspeed;

int state;
int control_case;
int print_case;

char testbuf[10];
unsigned long t;
unsigned long prevt;
unsigned long t0;
unsigned long tempt;
float t_ms;
float t_sec;
int mydelay;
int prevenc;
int posbreak1;
int negbreak1;
int posbreak2;
int negbreak2;

int v;
int e;
int preve;
float edot;

void setup()
{
  // Set up MPU 6050:
  Wire.begin();
  #if ARDUINO >= 157
  Wire.setClock(400000UL); // Set I2C frequency to 400kHz
  #else
    TWBR = ((F_CPU / 400000UL) - 16) / 2; // Set I2C frequency to 400kHz
  #endif

  
  Wire.beginTransmission(MPU_addr);
  Wire.write(0x6B);  // PWR_MGMT_1 register
  Wire.write(0);     // set to zero (wakes up the MPU-6050)
  Wire.endTransmission(true);

  //setup starting angle
  //1) collect the data
  Wire.beginTransmission(MPU_addr);
  Wire.write(0x3B);  // starting with register 0x3B (ACCEL_XOUT_H)
  Wire.endTransmission(false);
  Wire.requestFrom(MPU_addr,14,true);  // request a total of 14 registers
  AcX=Wire.read()<<8|Wire.read();  // 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)     
  AcY=Wire.read()<<8|Wire.read();  // 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
  AcZ=Wire.read()<<8|Wire.read();  // 0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)
  Tmp=Wire.read()<<8|Wire.read();  // 0x41 (TEMP_OUT_H) & 0x42 (TEMP_OUT_L)
  GyX=Wire.read()<<8|Wire.read();  // 0x43 (GYRO_XOUT_H) & 0x44 (GYRO_XOUT_L)
  GyY=Wire.read()<<8|Wire.read();  // 0x45 (GYRO_YOUT_H) & 0x46 (GYRO_YOUT_L)
  GyZ=Wire.read()<<8|Wire.read();  // 0x47 (GYRO_ZOUT_H) & 0x48 (GYRO_ZOUT_L)

  //2) calculate pitch and roll
  double roll = atan2(AcY, AcZ)*degconvert;
  double pitch = atan2(-AcX, AcZ)*degconvert;

  //3) set the starting angle to this pitch and roll
  double gyroXangle = roll;
  double gyroYangle = pitch;
  double compAngleX = roll;
  double compAngleY = pitch;


  Serial.begin(115200);
  Serial.print("minisegway new chassis");
  Serial.print('\n');
  Serial.print("sonar with MPU 6050 v. 1.2.0.3");
  Serial.print('\n');

  print_case = 0;
  
  pinMode(pwm_pin1, OUTPUT);
  pinMode(m1in1, OUTPUT);
  pinMode(m1in2, OUTPUT);

  pinMode(pwm_pin2, OUTPUT);
  pinMode(m2in1, OUTPUT);
  pinMode(m2in2, OUTPUT);


  //pinModeFast(isr_pin, OUTPUT);
  //digitalWrite(isr_pin, LOW);

  // encoder
  pinMode(enc1A, INPUT); 
  pinMode(enc1B, INPUT); 
  // turn on pullup resistors
  digitalWrite(enc1A, HIGH);
  digitalWrite(enc1B, HIGH);

  pinMode(enc2A, INPUT); 
  pinMode(enc2B, INPUT); 
  // turn on pullup resistors
  digitalWrite(enc2A, HIGH);
  digitalWrite(enc2B, HIGH);

  // encoder pin on interrupt 0 (pin 2)
  attachInterrupt(digitalPinToInterrupt(enc1A), doEncoder1, RISING);
  attachInterrupt(digitalPinToInterrupt(enc2A), doEncoder2, RISING);  

  n_loop = 10000;//must be higher than stop_n to start with 
  amp = 17;
  width = 0;
  stop_n = 200;//1000;
  n_delay = 10;
  a = 3.0;
  kd = 0.1;
  kp = a*kd;

  state = 0;
  pwm1 = 0;
  pwm2 = 0;
  
  posbreak1 = 0;
  posbreak2 = 0;
  negbreak1 = 0;
  negbreak2 = 0;

  preve = 0;

  //=======================================================
  // set up the Timer3 interrupt
  //=======================================================
  /* cli();          // disable global interrupts */
  /* TCCR3A = 0;     // set entire TCCR1A register to 0 */
  /* TCCR3B = 0;     // same for TCCR1B */

  /* // set compare match register to desired timer count: */
  /* //OCR1A = 15624; */
  /* OCR3A = 155;//100 Hz */
  /* // OCR1A = 100;//150ish - seems to work */
  /* //OCR1A = 77;//200 Hz <-- seems very borderline (might be 184 Hz) */
  /* //OCR1A = 30;//500 Hz */
  /* //OCR1A = 15;//1000 Hz */
  /* //OCR1A = 7;//2000 Hz */


  /* // turn on CTC mode: */
  /* TCCR3B |= (1 << WGM12); */

  /* // Set CS10 and CS12 bits for 1024 prescaler: */
  /* TCCR3B |= (1 << CS10); */
  /* TCCR3B |= (1 << CS12); */
 
  /* // enable timer compare interrupt: */
  /* TIMSK3 |= (1 << OCIE3A); */

  /* sei(); */
  //=======================================================
}

int inv_deadband(int speed, int posbreak, int negbreak){
  int out;
  out = speed;
  
  if ( out > 0){
    out += posbreak;
  }
  else if (out < 0){
    out += negbreak;
  }
  
  if (out < -255){
    out = -255;
  }
  else if (out > 255){
    out = 255;
  }

  return out;
}

void command_robot_speed(int speed){
  //int speed1;
  //int speed2;
  float scale;
  /* if (abs(speed) < 50){ */
  /*   scale = 3; */
  /* } */
  /* else if (abs(speed) < 75){ */
  /*   scale = 2; */
  /* } */
  /* else if (abs(speed) < 100){ */
  /*   scale = 1.5; */
  /* } */
  /* else{ */
  /*   scale = 1.2; */
  /* } */
  scale = 1.0;
  pwm1 = inv_deadband(-speed*scale, posbreak1, negbreak1);
  pwm2 = inv_deadband(speed, posbreak2, negbreak2);
  command_motor1(pwm1);
  command_motor2(pwm2);
}

/* void command_motor(int speed){ */

/*   speed = inv_deadband(speed); */

/*   if (speed > 0){ */
/*     digitalWrite(in1, LOW); */
/*     digitalWrite(in2, HIGH); */
/*     analogWrite(pwn_pin, speed); */
/*   } */
/*   else if (speed < 0){ */
/*     digitalWrite(in1, HIGH); */
/*     digitalWrite(in2, LOW); */
/*     analogWrite(pwn_pin, abs(speed)); */
/*   } */
/*   else { */
/*     digitalWrite(in1, LOW); */
/*     digitalWrite(in2, LOW); */
/*     analogWrite(pwn_pin, 0); */
/*   } */
/* } */

void command_motor(int speed, int pwm_pin, int in1, int in2){
  if (speed > 0){
    digitalWrite(in1, LOW);
    digitalWrite(in2, HIGH);
    analogWrite(pwm_pin, speed);
  }
  else if (speed < 0){
    digitalWrite(in1, HIGH);
    digitalWrite(in2, LOW);
    analogWrite(pwm_pin, abs(speed));
  }
  else {
    digitalWrite(in1, LOW);
    digitalWrite(in2, LOW);
    analogWrite(pwm_pin, 0);
  }
}

void command_motor1(int speed){
  command_motor(speed, pwm_pin1, m1in1, m1in2);
}

void command_motor2(int speed){
  command_motor(speed, pwm_pin2, m2in1, m2in2);
}

void cal_command_motor(int speed, int cal_case){
    if (cal_case == 1){
      command_motor1(speed);
    }
    else if (cal_case == 2){
      command_motor2(speed);
    }
}

void set_breakaways_case(int posbreak, int negbreak, int cal_case){
  if (cal_case == 1){
    posbreak1 = posbreak;
    negbreak1 = negbreak;
  }
  else if (cal_case == 2){
    posbreak2 = posbreak;
    negbreak2 = negbreak;
  }
}

void calibrate_deadband(int cal_case, volatile long *enc_count){
  int posbreak = 0;
  int negbreak = 0;
  prevenc = *enc_count;
  for (int a=0; a<200; a++){
    cal_command_motor(a, cal_case);
    delay(50);
    if (*enc_count != prevenc){
      Serial.print("posbreak amp = ");
      Serial.println(a);
      posbreak = a;
      break;
    }
  }
  cal_command_motor(0, cal_case);

  delay(100);
  prevenc = *enc_count;
  for (int a=0; a>-200; a--){
    cal_command_motor(a, cal_case);
    delay(50);
    if (*enc_count != prevenc){
      Serial.print("negbreak amp = ");
      Serial.println(a);
      negbreak = a;
      break;
    }
  }
  cal_command_motor(0, cal_case);

  set_breakaways_case(posbreak, negbreak, cal_case);
}

void preptest(){
  Serial.println("#====================");
  Serial.println("#raw_loop_count,n_loop, dt_ms, t_ms, pwm1, pwm2, enc_w1, enc_w2, sonar_diff, dt2, filt_sonar");
  delay(50);
  enc_count1 = 0;
  enc_count2 = 0;  
  n_loop = 0;
  nISR = 0;
  ISRstate = 0;
  fresh = 0;
  t0 = micros();
  prevt = t0;
  t = t0;
  dt2 = 0;
  print_case = 2;
}

int get_int(){
  int out_int;
  out_int = 0;
  while (out_int == 0){
    while (Serial.available() == 0){
      delay(10);
    }
    out_int = Serial.parseInt();
  }
  return(out_int);
  
}

float get_float(){
  float out_float;

  out_float = 0;
  while (out_float == 0){
    while (Serial.available() == 0){
      delay(10);
    }
    out_float = Serial.parseFloat();
  }
  return(out_float);
  
}

int read_one_byte(){
  int outbyte;

  while (Serial.available() == 0){
    delay(10);
  }
  outbyte = Serial.read();

  return(outbyte);
}

int get_valid_byte(){
  int outbyte;
  outbyte = read_one_byte();
  
  if (outbyte == 10 || outbyte == 13){
    //try again
    outbyte = read_one_byte();
  }
  
  return(outbyte);
}

void print_line_serial(){
  //labels: n_loop, dt_ms, t_ms, pwm1, pwm2, enc_w1, enc_w2, enc_pend
  Serial.print(raw_loop_count);//0
      Serial.print(",");    
      Serial.print(n_loop);//1
      Serial.print(",");    
      Serial.print(dt_ms);//2
      Serial.print(",");
      Serial.print(t_ms);//3
      Serial.print(",");    
      Serial.print(pwm1);//4
      Serial.print(",");    
      Serial.print(pwm2);//5
      Serial.print(",");    
      Serial.print(enc_count1);//6
      Serial.print(",");    
      Serial.print(enc_count2);//7
      Serial.print(",");    
      //Serial.print(enc_pend);
      Serial.print(d1);
      Serial.print(",");
      Serial.print(d2);
      Serial.print(",");
      Serial.print(sonar_diff);
      Serial.print(",");
      Serial.print(dt2);
      Serial.print(",");
      Serial.print(filt_sonar);
      Serial.print(",");
      Serial.print(compAngleY);
      Serial.print('\n');
}

int two_digits(float floatin){
  float float_part;
  int out_digits;
  float_part = floatin - (int)floatin;
  out_digits = (int)(100*float_part);
  return out_digits;
}

int three_digits(float floatin){
  float float_part;
  int out_digits;
  float_part = floatin - (int)floatin;
  out_digits = (int)(1000*float_part);
  return out_digits;
}

void print_line_sprintf(int pwm_out){
  char buffer[70];
  sprintf(buffer, "%d,%d.%0.3d,%d.%0.2d,%d,%d",
	  n_loop, (int)dt_ms, three_digits(dt_ms),
	  (int)t_ms, two_digits(t_ms),
	  enc_count1, pwm_out);
  Serial.println(buffer);
}

/* unsigned char getsecondbyte(int input){ */
/*     unsigned char output; */
/*     output = (unsigned char)(input >> 8); */
/*     return output; */
/* } */

 

/* int reassemblebytes(unsigned char msb, unsigned char lsb){ */
/*     int output; */
/*     output = (int)(msb << 8); */
/*     output += lsb; */
/*     return output; */
/* } */

int readtwobytes(void){
    unsigned char msb, lsb;
    int output;
    int iter = 0;
    while (Serial.available() <2){
      iter++;
      if (iter > 1e5){
	break;
      }
    }
    msb = Serial.read();
    lsb = Serial.read();
    output = reassemblebytes(msb, lsb);
    return output;
}

void SendTwoByteInt(int intin){
    unsigned char lsb, msb;
    lsb = (unsigned char)intin;
    msb = getsecondbyte(intin);
    Serial.write(msb);
    Serial.write(lsb);
}

void OL_pulse(int wheel){
  int pwm;
  if ( n_loop < n_delay ){
    // all cases start with a little bit of "off" time to
    // establish initial conditions
    pwm = 0;
  }
  else if ( n_loop < stop_n ){
    if (n_loop < (n_delay + width)){
      pwm = amp;
    }
    else{
      pwm = 0;
    }
  }

  if (wheel == 1){
    pwm1 = pwm;
    pwm2 = 0;
  }
  else if ( wheel == 2){
    pwm1 = 0;
    pwm2 = pwm;
  }
  command_motor1(pwm1);
  command_motor2(pwm2);    
}

void speed_pulse(){
  if ( n_loop < n_delay ){
    command_robot_speed(0);
  }
  else if (n_loop < (n_delay + width)){
    command_robot_speed(amp);
  }
  else{
    command_robot_speed(0);
  }
}


void PD_balancing(){
  /////e = sonar_diff-10;
  //e = sonar_diff-15;
  //e = filt_sonar;
  e = -compAngleY*12;//trying to scale to match sonar_diff
  edot = (e-preve)/dt_sec;
  v = (int)(kp*e + kd*edot);
  command_robot_speed(v);
  preve = e;//save for next time
}

void lead_comp(){
  v = Gz.calc_out(sonar_diff);
  command_robot_speed(v);
}


void loop()
{
   raw_loop_count++;

    prevt = t;//moved from in between the two lines below 03/29/17 11:50AM
    t = micros();
    n_loop++;
    dt_ms = (t-prevt)/1000.0;
    dt_sec = dt_ms/1000.0;
    t_ms = (t-t0)/1000.0;
    t_sec = t_ms/1000.0;

    //rrrrrrrrrrrrrrrrrrrrrrrrrrrrrr
    //
    // Read accel
    //
    //rrrrrrrrrrrrrrrrrrrrrrrrrrrrrr
    t1 = micros();

    //Collect raw data from the sensor.
    Wire.beginTransmission(MPU_addr);
    Wire.write(0x3B);  // starting with register 0x3B (ACCEL_XOUT_H)
    Wire.endTransmission(false);
    Wire.requestFrom(MPU_addr,14,true);  // request a total of 14 registers
    AcX=Wire.read()<<8|Wire.read();  // 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)     
    AcY=Wire.read()<<8|Wire.read();  // 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
    AcZ=Wire.read()<<8|Wire.read();  // 0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)
    Tmp=Wire.read()<<8|Wire.read();  // 0x41 (TEMP_OUT_H) & 0x42 (TEMP_OUT_L)
    GyX=Wire.read()<<8|Wire.read();  // 0x43 (GYRO_XOUT_H) & 0x44 (GYRO_XOUT_L)
    GyY=Wire.read()<<8|Wire.read();  // 0x45 (GYRO_YOUT_H) & 0x46 (GYRO_YOUT_L)
    GyZ=Wire.read()<<8|Wire.read();  // 0x47 (GYRO_ZOUT_H) & 0x48 (GYRO_ZOUT_L)

    //the next two lines calculate the orientation of the accelerometer relative to the earth and convert the output of atan2 from radians to degrees
    //We will use this data to correct any cumulative errors in the orientation that the gyroscope develops.
    double roll = atan2(AcY, AcZ)*degconvert;
    double pitch = atan2(-AcX, AcZ)*degconvert;
  
    //The gyroscope outputs angular velocities.  To convert these velocities from the raw data to deg/second, divide by 131.  
    //Notice, we're dividing by a double "131.0" instead of the int 131.
    double gyroXrate = GyX/131.0;
    double gyroYrate = GyY/131.0;

    //THE COMPLEMENTARY FILTER
    // This filter calculates the angle based MOSTLY on integrating
    // the angular velocity to an angular displacement.
    //
    // dt, recall, is the time between gathering data from the MPU6050.
    // We'll pretend that the angular velocity has remained constant
    // over the time dt, and multiply angular velocity by 
    // time to get displacement.
    // The filter then adds a small correcting factor from
    // the accelerometer ("roll" or "pitch"), so the gyroscope
    // knows which way is down.

    // Calculate the angle using a Complimentary filter
    compAngleX = 0.99 * (compAngleX + gyroXrate * dt_sec) + 0.01 * roll; 
    compAngleY = 0.99 * (compAngleY + gyroYrate * dt_sec) + 0.01 * pitch; 

    // ssssssssssssssssssssssssssss
    //
    // read sonar
    //
    //sssssssssssssssssssssssssssss
    
    // sonar code here
    d2 = sonar2.ping();
    //delay(2);
    delayMicroseconds(300);
    d1 = sonar1.ping();
    sonar_diff = d1-d2;
    filt_sonar = lpfilt.calc_out(sonar_diff);
    
    t2 = micros();
    dt2 = (t2-t1)/1000;
    
    //!//Serial.print("AcX = "); Serial.print(AcX);
    //!//Serial.print(" | AcY = "); Serial.print(AcY);
    //Serial.print(" W | AcZ = "); Serial.print(AcZ);
    //Serial.print(" | GyY = "); Serial.print(GyY);
    //Serial.print(" | dt = "); Serial.print(dt);


    //cccccccccccccccccccccccccccccccccccccc
    if ( control_case == 0){
      // motors off
      command_motor1(0);
      command_motor2(0);
    }
    else if (control_case == 1){
      //main PD case
      PD_balancing();
    }
    else if (control_case == 2){
      //OL pulse on wheel 1
      OL_pulse(1);
    }
    else if (control_case == 3){
      //OL pulse on wheel 2
      OL_pulse(2);
    }
    else if (control_case == 4){
      // robot speed pulse
      speed_pulse();
    }
    else if (control_case == 5){
      // debugging balancing control
      // - same as case 1, but turns off after n_loop reaches stop_n
      PD_balancing();
    }
    else if (control_case == 6){
      lead_comp();
    }
    else if (control_case == 7){
      //PD burst for debugging
      if ( n_loop < 100 ){
	PD_balancing();
      }
      else{
	command_motor1(0);
	command_motor2(0);
      }
      
    }


    // for all control cases other than 0 or 1, turn off the
    // control after n_loop reaches stop_n
    if ( control_case > 1 ){
      if ( n_loop > stop_n ){
	control_case = 0;
      }
    }
    //cccccccccccccccccccccccccccccccccccccc

    //pppppppppppppppppppppppppppppppppppppp
    // set print_case = 0 to turn printing off
    if (print_case == 1){
      print_line_serial();
    }
    else if (print_case == 2){
      if ( n_loop < stop_n ){
	print_line_serial();
      }
    }
    //pppppppppppppppppppppppppppppppppppppp

    

   // check serial
   if (Serial.available() > 0) {
    //digitalWrite(receivePin, HIGH);
    inByte = Serial.read();

    //if (inByte == 200){
      //// abort
      //control_case = 0;
      //print_case = 0;
      //}
    //else
    if (inByte == 1){
      // turn on balancing control
      control_case = 1;
    }
    else if (inByte == 2){
      delay(5);
      SendTwoByteInt(2);
    }
    else if (inByte == 3){
      // print data with motors off
      control_case = 0;
      preptest();// sets print_case = 2
    }
    else if (inByte == 4){
      // calibrate inverse deadband
      calibrate_deadband(1, &enc_count1);
      Serial.print("posbreak1: ");
      Serial.print(posbreak1);
      Serial.print('\n');
      Serial.print("negbreak1: ");
      Serial.print(negbreak1);
      Serial.print('\n');

      calibrate_deadband(2, &enc_count2);
      Serial.print("posbreak2: ");
      Serial.print(posbreak2);
      Serial.print('\n');
      Serial.print("negbreak2: ");
      Serial.print(negbreak2);
      Serial.print('\n');
    }
    else if (inByte == 5){
      //OL test case, one wheel
      //send_ser = true;
      amp = readtwobytes();
      width = readtwobytes();
      // extra param specifies which wheel to pulse
      extra_param = readtwobytes();
      control_case = extra_param + 1;//2 is pulse wheel 1, 3 is pulse wheel 2
      Serial.print("amp = ");
      Serial.print(amp);
      Serial.print(", width = ");
      Serial.print(width);
      Serial.print(", control_case = ");
      Serial.print(control_case);
      Serial.print('\n');
      preptest();
    }
    else if (inByte == 6){
      //OL speed test (forward or backward, depending on amp)
      //send_ser = true;
      amp = readtwobytes();
      width = readtwobytes();
      // extra param specifies which wheel to pulse
      control_case = 4;
      Serial.print("amp = ");
      Serial.print(amp);
      Serial.print(", width = ");
      Serial.print(width);
      Serial.print(", control_case = ");
      Serial.print(control_case);
      Serial.print('\n');
      preptest();
    }
    else if (inByte == 7){
      // PD balancing debug
      Serial.print("starting PD balancing test");
      control_case = 5;
      preptest();
    }
    else if (inByte == 8){
      // set Kp and Kd
      // receive kp and kd as integers which as the floats time 100
      extra_param = readtwobytes();
      kp = ((float)extra_param)/100.0;
      extra_param = readtwobytes();
      kd = ((float)extra_param)/1000.0;
      Serial.print("kp = ");
      Serial.print(kp);
      Serial.print('\n');
      Serial.print("kd*1000 = ");
      Serial.print(kd*1000);
      Serial.print('\n');
    }
    else if (inByte == 9){
      // set stop_n
      stop_n = readtwobytes();
    }
    else if (inByte == 10){
      // print one line of data for debugging purposes
      print_line_serial();
    }
    else if (inByte == 11){
      // system ID test
      control_case = 0;
      preptest();
    }
    else if (inByte == 12){
      // set b[2] and a[2] digcomp values
      // receive kp and kd as integers which as the floats time 100
      extra_param = readtwobytes();
      b_z[0] = ((float)extra_param)/100.0;
      extra_param = readtwobytes();
      b_z[1] = ((float)extra_param)/100.0;
      extra_param = readtwobytes();
      a_z[0] = ((float)extra_param)/100.0;
      extra_param = readtwobytes();
      a_z[1] = ((float)extra_param)/100.0;
      Serial.print("b_z[0] = ");
      Serial.print(b_z[0]);
      Serial.print('\n');
      Serial.print("b_z[1] = ");
      Serial.print(b_z[1]);
      Serial.print('\n');
      Serial.print("a_z[0] = ");
      Serial.print(a_z[0]);
      Serial.print('\n');
      Serial.print("a_z[1] = ");
      Serial.print(a_z[1]);
      Serial.print('\n');
    }
    else if (inByte == 13){
      // PD balancing debug
      Serial.print("starting digcomp balancing test");
      control_case = 6;
      preptest();
    }
    else if (inByte == 14){
      // PD balancing debug
      Serial.print("starting PD debug test");
      control_case = 7;
      preptest();
    }
    
    /* else if (inByte == 3){ */
    /*   //reset the pendulum encoder */
    /*   Wire.beginTransmission(8); // transmit to device #8 */
    /*   Wire.write(1);              // sends one byte */
    /*   Wire.endTransmission();   */
    /*   delay(5); */
    /*   SendTwoByteInt(3); */
    /* } */
   }

   // wait for end of dt
   /* tempt = micros(); */
   /* telapsed = tempt - t; */
   /* if (telapsed < goaldelay){ */
   /*   mydelay = goaldelay - telapsed; */
   /*   delayMicroseconds(mydelay); */
   /* } */
   //delay(5);
   //delay(2);
   delayMicroseconds(300);
}
 
// Interrupt service routines for the right motor's quadrature encoder
void doEncoder1()
{
  // Test transition; since the interrupt will only fire on 'rising' we don't need to read pin A
  //n++;

  _EncBSet1 = digitalRead(enc1B);   // read the input pin
  
  // and adjust counter + if A leads B
  if (_EncBSet1){
    enc_count1 ++;
  }
  else {
    enc_count1 --;
  }
}

void doEncoder2()
{
  // Test transition; since the interrupt will only fire on 'rising' we don't need to read pin A
  //n++;

  _EncBSet2 = digitalRead(enc2B);   // read the input pin
  
  // and adjust counter + if A leads B
  if (_EncBSet2){
    enc_count2 ++;
  }
  else {
    enc_count2 --;
  }
}

/* ISR(TIMER3_COMPA_vect) */
/* { */
/*   nISR++; */
/*   fresh = 1; */
/*   /\* if (ISRstate == 1){ *\/ */
/*   /\*   ISRstate = 0; *\/ */
/*   /\*   digitalWrite(sw_pin, LOW); *\/ */
/*   /\* } *\/ */
/*   /\* else{ *\/ */
/*   /\*   ISRstate = 1; *\/ */
/*   /\*   digitalWrite(sw_pin, HIGH); *\/ */
/*   /\* } *\/ */
/* } */

// function that executes whenever data is requested by master
// this function is registered as an event, see setup()
/* void requestEvent() { */
/*   //Wire.write(50); // respond with message of 6 bytes */
/*   //Wire.write(17); */
/*   SendTwoByteInt_i2c(encoder_count); */
/* } */


//void receiveEvent(int howMany) {
//  enc_pend = readtwobytes_i2c();
//  nISR++;
//  fresh = 1;
//  //Serial.println(x);         // print the integer
//}