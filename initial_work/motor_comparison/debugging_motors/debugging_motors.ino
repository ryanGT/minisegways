// Motor 1
int pwn_pin = 9;
int in1 = 2;
int in2 = 4;

#define encoderPinA 18
#define encoderPinB 16

//Motor 2
/* int pwn_pin = 10; */
/* int in1 = 7; */
/* int in2 = 8; */

/* #define encoderPinA 19 */
/* #define encoderPinB 17 */

//  encoder
volatile bool _EncoderBSet;
volatile long encoder_count = 0;

const byte mask = B11111000;
int prescale = 1;

int state;

int step_size;
int loops_per_step;

int n;
int n_loop;
int nISR;
int amp1;
int amp2;
int amp;
int width;
int n_start = 10;
int v;
int stop_n = 250;
unsigned long t0;
unsigned long t;
unsigned long t1;
int dt_micro;
int mydelay;
unsigned long prevt;
float dt_ms;
float dt_sec;
float t_ms;
float t_sec;

int inByte;
int fresh;

void setup()
{
  Serial.begin(115200);

  Serial.print("this might suck for a while");
  Serial.print("\n");

  pinMode(pwn_pin, OUTPUT);
  pinMode(in1, OUTPUT);
  pinMode(in2, OUTPUT);

  // encoder
  pinMode(encoderPinA, INPUT); 
  pinMode(encoderPinB, INPUT); 
  // turn on pullup resistors
  digitalWrite(encoderPinA, HIGH);
  digitalWrite(encoderPinB, HIGH);

  // encoder pin on interrupt 0 (pin 2)
  attachInterrupt(digitalPinToInterrupt(encoderPinA), doEncoder, RISING);

  //=======================================================
  // set up the Timer3 interrupt
  //=======================================================
  cli();          // disable global interrupts
  TCCR3A = 0;     // set entire TCCR1A register to 0
  TCCR3B = 0;     // same for TCCR1B

  // set compare match register to desired timer count:
  //OCR1A = 15624;
  OCR3A = 155;//100 Hz
  // OCR1A = 100;//150ish - seems to work
  //OCR1A = 77;//200 Hz <-- seems very borderline (might be 184 Hz)
  //OCR1A = 30;//500 Hz
  //OCR1A = 15;//1000 Hz
  //OCR1A = 7;//2000 Hz


  // turn on CTC mode:
  TCCR3B |= (1 << WGM12);

  // Set CS10 and CS12 bits for 1024 prescaler:
  TCCR3B |= (1 << CS10);
  TCCR3B |= (1 << CS12);
 
  // enable timer compare interrupt:
  TIMSK3 |= (1 << OCIE3A);

  sei();
  //=======================================================
  state = 0;

}

void command_motor(int speed){
  if (speed > 0){
    digitalWrite(in1, LOW);
    digitalWrite(in2, HIGH);
    analogWrite(pwn_pin, speed);
  }
  else if (speed < 0){
    digitalWrite(in1, HIGH);
    digitalWrite(in2, LOW);
    analogWrite(pwn_pin, abs(speed));
  }
  else {
    digitalWrite(in1, LOW);
    digitalWrite(in2, LOW);
    analogWrite(pwn_pin, 0);
  }
}


void mynewline(){
  Serial.print('\n');
}

int get_int(){
  int out_int;
  out_int = 0;
  while (out_int == 0){
    while (Serial.available() == 0){
      delay(10);
    }
    out_int = Serial.parseInt();
  }
  return(out_int);  
}

void menu(){
  Serial.println("input starting amplitude (example: 0)");
  mynewline();
  state = 0;// If something goes wrong, set code to return to menu

  while (Serial.available() == 0){
      delay(10);
  }

  amp1 = Serial.parseInt();
  Serial.println(amp1);
  
  Serial.println("input ending amplitude (example: 255)");
  
  while (Serial.available() == 0){
      delay(10);
  }

  amp2 = get_int();
  Serial.println(amp2);

  Serial.println("input step size (increment or decrement)");
  Serial.println("(example: 1)");
  
  while (Serial.available() == 0){
      delay(10);
  }

  step_size = get_int();
  Serial.println(step_size);
  
  Serial.println("how many loops per step (step duration in loop counts)");
  Serial.println("(example: 200)");
    
  while (Serial.available() == 0){
      delay(10);
  }

  loops_per_step = get_int();
  Serial.println(loops_per_step);
  
  // force the sign of step_size to be right
  if (amp2 > amp1){
    // increasing steps
    step_size = abs(step_size);
  }
  else if (amp2 < amp1){
    // decreasing steps
    step_size = -abs(step_size);
  }
  
  

  n_loop = 0;
  encoder_count = 0;
  fresh = 0;
  amp = amp1;
  state = 1;//state = 1 --> run test; state = 0 --> back to menu
  t0 = micros();
  Serial.println("starting test");
}


void loop()
{
  /* digitalWrite(in1, HIGH); */
  /* digitalWrite(in2, LOW); */
  /* analogWrite(pwn_pin,150); */
  /* delay(1000); */
  /* analogWrite(pwn_pin,0); */
  /* delay(100); */
  /* digitalWrite(in1, LOW); */
  /* digitalWrite(in2, HIGH); */
  /* analogWrite(pwn_pin,150); */
  /* delay(1000); */
  if ( state == 0){
    menu();
  }
    
  else if (state == 1){
    //Serial.println(nISR);
   if ( fresh == 1){
    fresh = 0;
    // the test is over when the stopping amplitude is reached,
    // but we have to handle incrementing or decrementing cases
    if ((step_size > 0) && (amp > amp2)){
      // this was an incrementing test and it is over
      command_motor(0);
      state = 0;// back to menu
    }
    else if ((step_size < 0) && (amp < amp2)){
      // this was a decrementing test and it is over
      command_motor(0);
      state = 0;
    }
    else{
      // the test is still going on      
      n_loop++;

      if (n_loop > loops_per_step){
	// go to the next step
	amp = amp + step_size;//step_size is signed, so this works for
	                      // incrementing or decrementing
	n_loop = 0;
      }

      t = micros()-t0;
      t_ms = t/1000.0;
      
      command_motor(amp);
      
      Serial.print(n_loop);
      Serial.print(",");
      Serial.print(t_ms);
      Serial.print(",");
      Serial.print(amp);
      Serial.print(",");
      Serial.print(encoder_count);
      mynewline();
    }
   }
  }
}


// Interrupt service routines for the right motor's quadrature encoder
void doEncoder()
{
  // Test transition; since the interrupt will only fire on 'rising' we don't need to read pin A
  //n++;

  _EncoderBSet = digitalRead(encoderPinB);   // read the input pin
  
  // and adjust counter + if A leads B
  if (_EncoderBSet){
    encoder_count ++;
  }
  else {
    encoder_count --;
  }
}



ISR(TIMER3_COMPA_vect)
{
  nISR++;
  fresh = 1;
  /* if (ISRstate == 1){ */
  /*   ISRstate = 0; */
  /*   digitalWrite(sw_pin, LOW); */
  /* } */
  /* else{ */
  /*   ISRstate = 1; */
  /*   digitalWrite(sw_pin, HIGH); */
  /* } */
}
