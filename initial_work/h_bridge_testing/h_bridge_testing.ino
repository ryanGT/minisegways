unsigned char getsecondbyte(int input){
  unsigned char output;
  output = (unsigned char)(input >> 8);
  return output;
}

int reassemblebytes(unsigned char msb, unsigned char lsb){
  int output;
  output = (int)(msb << 8);
  output += lsb;
  return output;
}

void mynewline(){
  Serial.print('\n');
}

unsigned long t1;
unsigned long t2;

// Encoder pins
// Verify that these are the same pins your encoder
// A and B channels are connected to
#define enc1A 2
#define enc1B 11

#define enc2A 3
#define enc2B 12

// The pwm, in1, and in2 connections are internal to the
// H-bridge shield
int pwm_pin1 = 9;
int m1in1 = 6;
int m1in2 = 4;

//pwm_pin2, m2in1, m2in2
int pwm_pin2 = 10;//<-- buy batteries and check these
int m2in1 = 7;//<-- buy batteries and check these
int m2in2 = 8;//<-- buy batteries and check these

//int isr_pin = A0;
int trig1 = 24;
int echo1 = 22;
int trig2 = 28;
int echo2 = 26;

//  encoder
volatile bool _EncBSet1;
volatile long enc_count1 = 0;

volatile bool _EncBSet2;
volatile long enc_count2 = 0;

volatile int enc_pend = 0;

int n_loop, stop_n, n_delay, nISR;
long raw_loop_count=0;

const byte mask = B11111000;
int prescale = 1;

int fresh;
int ISRstate;

#define offset 0

int inByte;
int outByte;

int extra_param;
int amp;
int amp1;
int amp2;
float freq;
float w;
float theta_d;
float dt_ms;
float dt1;
float dt2;
float dt_sec;
float kp;
float kd;
float a;
int pwm1;
int pwm2;
int width;
int curspeed;

int state;
int control_case;
int print_case;

char testbuf[10];
unsigned long t;
unsigned long prevt;
unsigned long t0;
unsigned long tempt;
float t_ms;
float t_sec;
int mydelay;
int prevenc;
int posbreak1;
int negbreak1;
int posbreak2;
int negbreak2;

int v;
int e;
int preve;
float edot;

void setup()
{
  Serial.begin(115200);
  Serial.print("motor testing v. 1.0.0");
  Serial.print('\n');

  print_case = 0;
  
  pinMode(pwm_pin1, OUTPUT);
  pinMode(m1in1, OUTPUT);
  pinMode(m1in2, OUTPUT);

  pinMode(pwm_pin2, OUTPUT);
  pinMode(m2in1, OUTPUT);
  pinMode(m2in2, OUTPUT);


  //pinModeFast(isr_pin, OUTPUT);
  //digitalWrite(isr_pin, LOW);

  // encoder
  pinMode(enc1A, INPUT); 
  pinMode(enc1B, INPUT); 
  // turn on pullup resistors
  digitalWrite(enc1A, HIGH);
  digitalWrite(enc1B, HIGH);

  pinMode(enc2A, INPUT); 
  pinMode(enc2B, INPUT); 
  // turn on pullup resistors
  digitalWrite(enc2A, HIGH);
  digitalWrite(enc2B, HIGH);

  // encoder pin on interrupt 0 (pin 2)
  attachInterrupt(digitalPinToInterrupt(enc1A), doEncoder1, RISING);
  attachInterrupt(digitalPinToInterrupt(enc2A), doEncoder2, RISING);  

  n_loop = 10000;//must be higher than stop_n to start with 
  amp = 17;
  width = 0;
  stop_n = 200;//1000;
  n_delay = 10;
  a = 3.0;
  kd = 0.1;
  kp = a*kd;

  state = 0;
  pwm1 = 0;
  pwm2 = 0;
  
  posbreak1 = 0;
  posbreak2 = 0;
  negbreak1 = 0;
  negbreak2 = 0;

  preve = 0;

  //=======================================================
  // set up the Timer3 interrupt
  //=======================================================
  /* cli();          // disable global interrupts */
  /* TCCR3A = 0;     // set entire TCCR1A register to 0 */
  /* TCCR3B = 0;     // same for TCCR1B */

  /* // set compare match register to desired timer count: */
  /* //OCR1A = 15624; */
  /* OCR3A = 155;//100 Hz */
  /* // OCR1A = 100;//150ish - seems to work */
  /* //OCR1A = 77;//200 Hz <-- seems very borderline (might be 184 Hz) */
  /* //OCR1A = 30;//500 Hz */
  /* //OCR1A = 15;//1000 Hz */
  /* //OCR1A = 7;//2000 Hz */


  /* // turn on CTC mode: */
  /* TCCR3B |= (1 << WGM12); */

  /* // Set CS10 and CS12 bits for 1024 prescaler: */
  /* TCCR3B |= (1 << CS10); */
  /* TCCR3B |= (1 << CS12); */
 
  /* // enable timer compare interrupt: */
  /* TIMSK3 |= (1 << OCIE3A); */

  /* sei(); */
  //=======================================================
}

void command_motor(int speed, int pwm_pin, int in1, int in2){
  if (speed > 0){
    digitalWrite(in1, LOW);
    digitalWrite(in2, HIGH);
    analogWrite(pwm_pin, speed);
  }
  else if (speed < 0){
    digitalWrite(in1, HIGH);
    digitalWrite(in2, LOW);
    analogWrite(pwm_pin, abs(speed));
  }
  else {
    digitalWrite(in1, LOW);
    digitalWrite(in2, LOW);
    analogWrite(pwm_pin, 0);
  }
}

void command_motor1(int speed){
  command_motor(speed, pwm_pin1, m1in1, m1in2);
}

void command_motor2(int speed){
  command_motor(speed, pwm_pin2, m2in1, m2in2);
}

void cal_command_motor(int speed, int cal_case){
    if (cal_case == 1){
      command_motor1(speed);
    }
    else if (cal_case == 2){
      command_motor2(speed);
    }
}

void preptest(){
  Serial.println("#====================");
  Serial.println("#raw_loop_count,n_loop, dt_ms, t_ms, pwm1, pwm2, enc_w1, enc_w2");
  delay(50);
  enc_count1 = 0;
  enc_count2 = 0;  
  n_loop = 0;
  nISR = 0;
  ISRstate = 0;
  fresh = 0;
  t0 = micros();
  prevt = t0;
  t = t0;
  dt2 = 0;
  print_case = 2;
}

int get_int(){
  int out_int;
  out_int = 0;
  while (out_int == 0){
    while (Serial.available() == 0){
      delay(10);
    }
    out_int = Serial.parseInt();
  }
  return(out_int);
  
}

float get_float(){
  float out_float;

  out_float = 0;
  while (out_float == 0){
    while (Serial.available() == 0){
      delay(10);
    }
    out_float = Serial.parseFloat();
  }
  return(out_float);
  
}

int read_one_byte(){
  int outbyte;

  while (Serial.available() == 0){
    delay(10);
  }
  outbyte = Serial.read();

  return(outbyte);
}

int get_valid_byte(){
  int outbyte;
  outbyte = read_one_byte();
  
  if (outbyte == 10 || outbyte == 13){
    //try again
    outbyte = read_one_byte();
  }
  
  return(outbyte);
}

void print_line_serial(){
  //labels: n_loop, dt_ms, t_ms, pwm1, pwm2, enc_w1, enc_w2, enc_pend
  Serial.print(raw_loop_count);//0
      Serial.print(",");    
      Serial.print(n_loop);//1
      Serial.print(",");    
      Serial.print(dt_ms);//2
      Serial.print(",");
      Serial.print(t_ms);//3
      Serial.print(",");    
      Serial.print(pwm1);//4
      Serial.print(",");    
      Serial.print(pwm2);//5
      Serial.print(",");    
      Serial.print(enc_count1);//6
      Serial.print(",");    
      Serial.print(enc_count2);//7
      //Serial.print(enc_pend);
      Serial.print('\n');
}

int two_digits(float floatin){
  float float_part;
  int out_digits;
  float_part = floatin - (int)floatin;
  out_digits = (int)(100*float_part);
  return out_digits;
}

int three_digits(float floatin){
  float float_part;
  int out_digits;
  float_part = floatin - (int)floatin;
  out_digits = (int)(1000*float_part);
  return out_digits;
}

void print_line_sprintf(int pwm_out){
  char buffer[70];
  sprintf(buffer, "%d,%d.%0.3d,%d.%0.2d,%d,%d",
	  n_loop, (int)dt_ms, three_digits(dt_ms),
	  (int)t_ms, two_digits(t_ms),
	  enc_count1, pwm_out);
  Serial.println(buffer);
}

/* unsigned char getsecondbyte(int input){ */
/*     unsigned char output; */
/*     output = (unsigned char)(input >> 8); */
/*     return output; */
/* } */

 

/* int reassemblebytes(unsigned char msb, unsigned char lsb){ */
/*     int output; */
/*     output = (int)(msb << 8); */
/*     output += lsb; */
/*     return output; */
/* } */

int readtwobytes(void){
    unsigned char msb, lsb;
    int output;
    int iter = 0;
    while (Serial.available() <2){
      iter++;
      if (iter > 1e5){
	break;
      }
    }
    msb = Serial.read();
    lsb = Serial.read();
    output = reassemblebytes(msb, lsb);
    return output;
}

void SendTwoByteInt(int intin){
    unsigned char lsb, msb;
    lsb = (unsigned char)intin;
    msb = getsecondbyte(intin);
    Serial.write(msb);
    Serial.write(lsb);
}

void menu(){
  Serial.println("input test case");
  mynewline();
  state = 0;// If something goes wrong, set code to return to menu

  while (Serial.available() == 0){
      delay(10);
  }

  inByte = Serial.read();

  if ( inByte == '1' ){
    amp1 = 200;
    amp2 = 0;
  }
  else if ( inByte == '2' ){
    amp1 = -200;
    amp2 = 0;
  }
  else if ( inByte == '3' ){
    amp1 = 0;
    amp2 = 200;
  }
  else if ( inByte == '4' ){
    amp1 = 0;
    amp2 = -200;
  }
  else if ( inByte == '5' ){
    amp1 = 200;
    amp2 = 200;
  }
  else if ( inByte == '6' ){
    amp1 = -200;
    amp2 = -200;
  }
  state = 1;
  n_loop = 0;
  preptest();
}

void loop()
{
  if ( state == 0){
      menu();
  }
      
  else{
    if (n_loop == 0){
      t0 = micros();
    }

    prevt = t;//moved from in between the two lines below 03/29/17 11:50AM
    t = micros();
    n_loop++;
    dt_ms = (t-prevt)/1000.0;
    dt_sec = dt_ms/1000.0;
    t_ms = (t-t0)/1000.0;
    t_sec = t_ms/1000.0;

    if ( (n_loop > 10) && ( n_loop < 100 )){
        pwm1 = amp1;
	pwm2 = amp2;
	command_motor1(amp1);
	command_motor2(amp2);
      }
    else{
        pwm1 = 0;
	pwm2 = 0;
	command_motor1(0);
	command_motor2(0);
    }
    print_line_serial();

    delayMicroseconds(300);

   if ( n_loop > stop_n){
     state = 0;
   }
 }
}  
// Interrupt service routines for the right motor's quadrature encoder
void doEncoder1()
{
  // Test transition; since the interrupt will only fire on 'rising' we don't need to read pin A
  //n++;

  _EncBSet1 = digitalRead(enc1B);   // read the input pin
  
  // and adjust counter + if A leads B
  if (_EncBSet1){
    enc_count1 ++;
  }
  else {
    enc_count1 --;
  }
}

void doEncoder2()
{
  // Test transition; since the interrupt will only fire on 'rising' we don't need to read pin A
  //n++;

  _EncBSet2 = digitalRead(enc2B);   // read the input pin
  
  // and adjust counter + if A leads B
  if (_EncBSet2){
    enc_count2 ++;
  }
  else {
    enc_count2 --;
  }
}

/* ISR(TIMER3_COMPA_vect) */
/* { */
/*   nISR++; */
/*   fresh = 1; */
/*   /\* if (ISRstate == 1){ *\/ */
/*   /\*   ISRstate = 0; *\/ */
/*   /\*   digitalWrite(sw_pin, LOW); *\/ */
/*   /\* } *\/ */
/*   /\* else{ *\/ */
/*   /\*   ISRstate = 1; *\/ */
/*   /\*   digitalWrite(sw_pin, HIGH); *\/ */
/*   /\* } *\/ */
/* } */

// function that executes whenever data is requested by master
// this function is registered as an event, see setup()
/* void requestEvent() { */
/*   //Wire.write(50); // respond with message of 6 bytes */
/*   //Wire.write(17); */
/*   SendTwoByteInt_i2c(encoder_count); */
/* } */


//void receiveEvent(int howMany) {
//  enc_pend = readtwobytes_i2c();
//  nISR++;
//  fresh = 1;
//  //Serial.println(x);         // print the integer
//}